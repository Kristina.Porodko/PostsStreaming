
val dependencies = Seq(
  "io.confluent" % "kafka-avro-serializer" % "3.3.0",
  "org.apache.kafka" % "kafka_2.11" % "0.10.2.1",
  "com.sksamuel.avro4s" % "avro4s-core_2.11" % "1.6.4",
  "com.github.scopt" % "scopt_2.11" % "3.7.0",
  "com.google.guava" % "guava" % "23.0",
  "org.apache.kafka" % "kafka-streams" % "0.11.0.0",
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.9.1"
)

lazy val root = (project in file(".")).settings(
  name := "PostsStreaming",
  version := "0.1",
  scalaVersion := "2.11.8",
  resolvers += "Confluent Maven Repo" at "http://packages.confluent.io/maven/",
  libraryDependencies ++= dependencies
)

