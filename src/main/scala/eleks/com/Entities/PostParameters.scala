package eleks.com.Entities

case class PostParameters(var postId : Int = 0,
                          var likeCount: Int = 0,
                          var dislikeCount : Int = 0,
                          var repostCount : Int = 0
                         )
{
  def + (post : PostParameters) : PostParameters = {
    PostParameters(this.postId,
      this.likeCount + post.likeCount,
      this.dislikeCount + post.dislikeCount,
      this.repostCount + post.repostCount)
  }
}