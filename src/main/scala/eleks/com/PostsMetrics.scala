package eleks.com

import Entities.{Action, PostParameters}
import Serializers.{AvroSerde, JSONSerde}
import com.sksamuel.avro4s.RecordFormat
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.common.serialization.{Serde, Serdes}
import org.apache.kafka.streams.kstream._
import org.apache.kafka.streams.processor.StateStore
import org.apache.kafka.streams.{KafkaStreams, KeyValue, StreamsConfig}
import java.util.Properties
import java.util.concurrent.TimeUnit

import scala.collection.mutable

object PostsMetrics extends Runnable{
  var streams: KafkaStreams = null
  val streamBuilder: KStreamBuilder = new KStreamBuilder()

  val inputFormatter: RecordFormat[Action] = RecordFormat[Action]
  val outputFormatter: RecordFormat[PostParameters] = RecordFormat[PostParameters]

  class PostParametersKeyValueMapper extends KeyValueMapper[Int, GenericRecord, KeyValue[Int, GenericRecord]] {
    override def apply(actionId: Int, actionRecord: GenericRecord): KeyValue[Int, GenericRecord] = {
      val postId = inputFormatter.from(actionRecord).postid
      new KeyValue(postId, actionRecord)
    }
  }

  class PostParametersInitializer extends Initializer[mutable.Map[Int, PostParameters]] {
    override def apply(): mutable.Map[Int, PostParameters] = {
      mutable.Map()
    }
  }

  class PostStatsAggregator extends Aggregator[Int, GenericRecord, mutable.Map[Int, PostParameters]] {
    override def apply(postId: Int, actionRecord: GenericRecord, aggregate: mutable.Map[Int, PostParameters]): mutable.Map[Int, PostParameters] = {
      val action = inputFormatter.from(actionRecord)

      def increment(stats : PostParameters, action : Action): Unit = {
        action.action match {
          case "Like" => stats.likeCount += 1
          case "Dislike" => stats.dislikeCount += 1
          case "Repost" => stats.repostCount += 1
          case default => {}
        }
      }

      if(aggregate.contains(postId)) {
        increment(aggregate(postId), action)
      } else {
        val stats = PostParameters(postId.toInt)
        increment(stats, action)
        aggregate(postId) = stats
      }

      aggregate
    }
  }

  class PostParametersRecordMapper extends KeyValueMapper[Int, mutable.Map[Int, PostParameters], KeyValue[Int, GenericRecord]] {
    override def apply(postId: Int, postStatsMap: mutable.Map[Int, PostParameters]): KeyValue[Int, GenericRecord] = {
      val outputValue = outputFormatter.to(postStatsMap(postId))
      new KeyValue(postId, outputValue)
    }
  }

  def transform(stream: KStream[Int, GenericRecord]): KStream[Int, GenericRecord] = {
    stream.map[Int, GenericRecord](new PostParametersKeyValueMapper)
      .groupByKey()
      .aggregate[mutable.Map[Int, PostParameters]](
      new PostParametersInitializer,
      new PostStatsAggregator,
      new JSONSerde[mutable.Map[Int, PostParameters]]
    )
      .toStream()
      .map[Int, GenericRecord](new PostParametersRecordMapper)
  }

  def configure(properties: Properties): Unit = {
    //TODO: take names of topics from some configs
    val actionsStream: KStream[Int, GenericRecord] = streamBuilder.stream("actions")

    val outputStream: KStream[Int, GenericRecord] = transform(actionsStream)
    outputStream.to("postMetrics")

    this.streams = new KafkaStreams(streamBuilder, properties)
  }

  override def run(){
    streams.start()

    Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
      override def run(): Unit = {
        streams.close(10, TimeUnit.SECONDS)
      }
    }))
  }

}

